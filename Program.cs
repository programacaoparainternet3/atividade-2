using System;
using System.Collections.Generic;
using System.Linq;

namespace Lambda
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> ListaNomes = new List<string>();
            ListaNomes.Add("Eduardo");
            ListaNomes.Add("Fernando");
            ListaNomes.Add("Maria");
            ListaNomes.Add("Joao");
            ListaNomes.Add("Eva");
            ListaNomes.Add("Mirian");
            ListaNomes.ForEach(n => TransformaUpperCase(n));

            List<int> listaNumeros = new List<int>();
            listaNumeros.Add(4);
            listaNumeros.Add(6);
            listaNumeros.Add(7);
            listaNumeros.Add(34);
            listaNumeros.Add(33);
            listaNumeros.Add(26);
            listaNumeros.Add(3);
            
            listaNumeros.ForEach(number => NumerosPares(number));

            int[] numbers = { 17, 4, 13, 3, 9, 22, 42, 7, 46, 1, 0 };
            int impares = numbers.Count(n => n % 2 == 1);

            Console.WriteLine("Total de Numeros Impares");
            Console.WriteLine(impares);
        }

        private static void NumerosPares(int number)
        {
            int num = (number % 2 == 0) == true ? number : -1;
            if (num > 0)
            {
                Console.WriteLine(num);
            }
        }

        private static void TransformaUpperCase(string n)
        {
            Console.WriteLine(n.ToUpper());
        }
    }
}
